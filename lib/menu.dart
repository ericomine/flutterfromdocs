import 'package:flutter/material.dart';

class MenuScreen extends StatefulWidget {
  final username;
  MenuScreen({
    Key key,
    @required this.username
  }) : super(key: key);

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Menu"),
        ),
        body: Container(
          child: Center(
            child: Text("Hello ${widget.username}"),
          ),
        ),
      ),
    );
  }
}
