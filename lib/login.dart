import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  Widget logo() => AspectRatio(
        aspectRatio: 1.0,
        child: Container(
          color: Colors.blue,
        ),
      );

  Widget textFields(usernameController, passwordController) {
    return Column(
      children: <Widget>[
        SizedBox(height: 16.0),
        TextField(
          controller: usernameController,
          decoration: InputDecoration(
            filled: true,
            labelText: "Username",
          ),
        ),
        SizedBox(height: 16.0),
        TextField(
          controller: passwordController,
          decoration: InputDecoration(
            filled: true,
            labelText: "Password",
          ),
          obscureText: true,
        ),
      ],
    );
  }

  Widget buttonBar() => ButtonBar(
        children: <Widget>[
          FlatButton(
            child: Text("Clear"),
            onPressed: _clearTextFields,
          ),
          RaisedButton(
              child: Text("Login"), onPressed: () => _handleLogin(context)),
        ],
      );

  void _handleLogin(context) {
    final username = _usernameController.text;
    Navigator.pushReplacementNamed(context, "/menu", arguments: username);
  }

  void _clearTextFields() {
    _usernameController.clear();
    _passwordController.clear();
  }

  @override
  void initState() {
    _usernameController.addListener(() => print(_usernameController.text));
    _passwordController.addListener(() => print(_passwordController.text));
    super.initState();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Material(
            child: Container(
              padding: EdgeInsets.all(0.2 * screenWidth),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  logo(),
                  textFields(_usernameController, _passwordController),
                  buttonBar(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
