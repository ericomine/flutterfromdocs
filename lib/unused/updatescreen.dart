// https://stackoverflow.com/questions/57430474/flutter-texteditingcontroller-only-static-member-can-be-accessed-in-initialize/61238899#61238899

import 'package:flutter/material.dart';

class UpdateScreen extends StatefulWidget {
  final String idUpdate;
  final String nrpUpdate;
  final String namaUpdate;
  final String emailUpdate;
  final String jurusanUpdate;

  UpdateScreen(
      {this.idUpdate,
      this.nrpUpdate,
      this.namaUpdate,
      this.emailUpdate,
      this.jurusanUpdate});

  @override
  _UpdateScreenState createState() => _UpdateScreenState();
}

class _UpdateScreenState extends State<UpdateScreen> {
  TextEditingController _txtnrp;
  int _count = 1;

  // This is an example to illustrate undesired behaviour:
  // Whenever clicking the button sets state to increment _count,
  // this will reassign the TextEditingController.text to hard coded value.

  Widget getColumn(someText) {
    print(someText);
    return Column(
      children: <Widget>[
        TextField(
          controller: _txtnrp, // this works
          // this doesn't:
          // controller: TextEditingController(text: someText),
          keyboardType: TextInputType.number,
          decoration: InputDecoration(hintText: "NRP"),
          textAlign: TextAlign.center,
        ),
        FlatButton(
          child: Text("Click"),
          onPressed: () => setState(() {_count += 1;})
        )
      ],
    );
  }

  @override
  void initState() {
    _txtnrp = TextEditingController(text: "${widget.nrpUpdate}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_left),
          onPressed: () => Navigator.pop(context, false),
        ),
        centerTitle: true,
        title: Text('Update ${widget.namaUpdate}'),
      ),
      body: Container(
        child: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.all(20),
            scrollDirection: Axis.vertical,
            child: getColumn("${widget.nrpUpdate}")
          ),
        ),
      ),
    );
  }
}
