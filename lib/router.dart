import 'package:flutter/material.dart';
import 'package:fromdocs/login.dart';
import 'package:fromdocs/menu.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    //var args = settings.arguments;

    switch (settings.name) {
      case "/":
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case "/menu":
        var args = 10;
        if (args is String) {
          return MaterialPageRoute(builder: (_) => MenuScreen(username: args));
        }
        return _errorRoute("Argument passed is not string.");
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute([errorText = "Invalid Route."]) {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        body: Center(
          child: Text(errorText),
        ),
      );
    });
  }
}
